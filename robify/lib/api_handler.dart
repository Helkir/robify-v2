import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:robify/Models/artist.dart';
import 'package:robify/Models/auth_token.dart';

class ApiHandler {
  Future<AuthToken> fetchToken(String name, String mdp) async {
    final response = await http.post(
      Uri.parse('https://music.gryt.tech/api-token-auth/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'username': name,
        'password': mdp,
      }),
    );

    if (response.statusCode == 201 || response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      return AuthToken.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception. ///TODO HANDLE EXCEPTION
      throw Exception('Failed to auth .');
    }
  }

  Future<List<Artist>> fetchArtists() async {
    final authToken = await fetchToken('arnaud',
        '1EsVsbuqhk8jVDDV'); // time to cache token after login instead
    String tokenValue = authToken.token;
    final response = await http
        .get(Uri.parse('https://music.gryt.tech/api/artists/'), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token $tokenValue',
    });
    if (response.statusCode == 201 || response.statusCode == 200) {
      Iterable data = json.decode(response.body);
      List<Artist> artists =
          List<Artist>.from(data.map((model) => Artist.fromJson(model)));

      return artists;
    } else {
      var toto = response.statusCode;
      throw Exception('Failed to load Artists. : $toto');
    }
  }
}
