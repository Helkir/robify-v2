import 'package:flutter/material.dart';
import 'package:robify/Components/most_listened_bloc.dart';
import 'package:robify/Components/small_playlist_bloc.dart';
import 'package:robify/Components/small_stylizers.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            const SmallSpacer(spacing: 16, spacerType: SpacerType.vertical),
            Row(
              children: [
                const Text(
                  "Welcome",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
                const Spacer(),
                Row(
                  children: [
                    Image.asset(
                      'images/icon_bell.png',
                      height: 27,
                      width: 27,
                    ),
                    const SmallSpacer(
                        spacing: 8, spacerType: SpacerType.horizontal),
                    Image.asset(
                      'images/icon_history.png',
                      height: 27,
                      width: 27,
                    ),
                    const SmallSpacer(
                        spacing: 8, spacerType: SpacerType.horizontal),
                    Image.asset(
                      'images/icon_gear.png',
                      height: 27,
                      width: 27,
                    ),
                  ],
                ),
              ],
            ),
            const SmallSpacer(spacing: 32, spacerType: SpacerType.vertical),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SmallPlaylistbloc(),
                    SmallSpacer(spacing: 16, spacerType: SpacerType.horizontal),
                    SmallPlaylistbloc(),
                  ],
                ),
                const SmallSpacer(spacing: 16, spacerType: SpacerType.vertical),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SmallPlaylistbloc(),
                    SmallSpacer(spacing: 16, spacerType: SpacerType.horizontal),
                    SmallPlaylistbloc(),
                  ],
                ),
                const SmallSpacer(spacing: 16, spacerType: SpacerType.vertical),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SmallPlaylistbloc(),
                    SmallSpacer(spacing: 16, spacerType: SpacerType.horizontal),
                    SmallPlaylistbloc(),
                  ],
                ),
              ],
            ),
            const SmallSpacer(spacing: 32, spacerType: SpacerType.vertical),
            const Text(
              "Most Listened",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontSize: 24,
              ),
            ),
            const SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: MostListenedBloc(),
            ),
            const Text(
              // TODO REMOVE
              "check only if scrolls work",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 124,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
