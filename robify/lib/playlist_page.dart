import 'package:flutter/material.dart';

class PlaylistPage extends StatelessWidget {
  const PlaylistPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: () {
          debugPrint("PLAYLIST !");
        },
        child: const Text('PLAYLIST'),
      ),
    );
  }
}
