import 'package:flutter/material.dart';
import 'package:robify/Components/small_stylizers.dart';
import 'package:robify/api_handler.dart';
import 'package:robify/Models/artist.dart';

class LibraryPage extends StatefulWidget {
  const LibraryPage({Key? key}) : super(key: key);

  @override
  State<LibraryPage> createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> {
  var apiHandler = ApiHandler();
  List<Artist> futureArtists = [];

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        const Text(
          "Library",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
            fontSize: 24,
          ),
        ),
        const SmallSpacer(spacing: 32, spacerType: SpacerType.vertical),
        ElevatedButton(
            onPressed: () async {
              futureArtists = await apiHandler.fetchArtists();

              apiHandler.fetchArtists().then((List<Artist> artists) {
                setState(() {
                  futureArtists = artists;
                });
              });
            },
            child: const Text('Load Artists !')),
        const SmallSpacer(spacing: 32, spacerType: SpacerType.vertical),
        SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              const Text('Artists'),
              if (futureArtists.isNotEmpty)
                for (int i = 0; i < futureArtists.length; i++)
                  LibraryBloc(artist: futureArtists[i]),
            ],
          ),
        ),
      ],
    );
  }
}

class LibraryBloc extends StatelessWidget {
  final Artist artist;

  const LibraryBloc({Key? key, required this.artist}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.network(
          artist.albumCoverUrl != ""
              ? artist.albumCoverUrl
              : 'images/flutter.jpg',
          fit: BoxFit.scaleDown,
          height: 64,
          width: 64,
        ),
        const SmallSpacer(spacing: 16, spacerType: SpacerType.horizontal),
        Column(
          children: [
            Text(
              artist.name != "" ? artist.name : 'No name in DB',
              style: const TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
            Text(
              artist.genreName != "" ? artist.genreName : 'No genre in DB',
              style: const TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
          ],
        ),
        const Spacer(),
        Image.asset(
          'images/icon_play.png',
          height: 27,
          width: 27,
        ),
        const SmallSpacer(spacing: 16, spacerType: SpacerType.vertical),
      ],
    );
  }
}
