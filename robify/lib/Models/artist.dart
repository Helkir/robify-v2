class Artist {
  final int id;
  final String name;
  final String genreName;
  final String albumCoverUrl;

  const Artist({
    required this.id,
    required this.name,
    required this.genreName,
    required this.albumCoverUrl,
  });

  factory Artist.fromJson(Map<String, dynamic> json) {
    return Artist(
      id: json['id'],
      name: json['name'],
      genreName: json['genre_name'],
      albumCoverUrl: json['album_cover_url'],
    );
  }
}
