import 'package:flutter/material.dart';
import 'package:robify/Models/auth_token.dart';
import 'package:robify/api_handler.dart';
import 'package:robify/super_view.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: LoginBloc(),
    );
  }
}

class LoginBloc extends StatefulWidget {
  const LoginBloc({Key? key}) : super(key: key);

  @override
  State<LoginBloc> createState() => _LoginBlocState();
}

class _LoginBlocState extends State<LoginBloc> {
  AuthToken? futureAuthToken;
  String _mdp = '';
  String _user = '';

  final _formKey = GlobalKey<FormState>();
  ApiHandler apiHandler = ApiHandler();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Scaffold(
      appBar: null,
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: Image.asset('images/robify.png'),
              ),
              TextFormField(
                onChanged: (login) {
                  _user = login;
                },
                style: const TextStyle(color: Colors.green),
                decoration: InputDecoration(
                  hintText: 'User',
                  hintStyle: const TextStyle(color: Colors.white),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50)),
                ),

                // The validator receives the text that the user has entered.
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
              TextFormField(
                onChanged: (password) {
                  _mdp = password;
                },
                obscureText: true,
                style: const TextStyle(color: Colors.green),
                decoration: InputDecoration(
                  hintText: 'Password',
                  hintStyle: const TextStyle(color: Colors.white),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50)),
                ),
                // The validator receives the text that the user has entered.
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
              ElevatedButton(
                style: ButtonStyle(
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                ),
                onPressed: () async {
                  // Validate returns true
                  //shif the form is valid, or false otherwise.
                  if (_formKey.currentState!.validate()) {
                    futureAuthToken = await apiHandler.fetchToken(_user, _mdp);

                    if (futureAuthToken != null) {
                      // TODO REFACTO ET REAL TEST
                      // TODO STORE INTO SHARED
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return const SuperView();
                          },
                        ),
                      );
                    }
                  }
                },
                child: const Text('Submit'),
              ),
              ElevatedButton(
                style: ButtonStyle(
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                ),
                onPressed: () async {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) {
                        return const SuperView();
                      },
                    ),
                  );
                },
                child: const Text('DEV LOGIN'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
