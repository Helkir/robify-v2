import 'package:flutter/material.dart';
import 'package:robify/login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
        scaffoldBackgroundColor: Colors.black, // doesnt work somehow ?
        backgroundColor: Colors.black, // same
      ),
      debugShowCheckedModeBanner: false,
      home: const LoginPage(),
    );
  }
}
