import 'package:flutter/material.dart';

class MostListenedBloc extends StatelessWidget {
  const MostListenedBloc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: const [
        //ptit foreach ici avec space
        MostListenedItem(),
        MostListenedItem(),
        MostListenedItem(),
        MostListenedItem(),
        MostListenedItem(),
        MostListenedItem(),
        MostListenedItem(),
        MostListenedItem(),
        MostListenedItem(),
        MostListenedItem(),
      ],
    );
  }
}

class MostListenedItem extends StatelessWidget {
  const MostListenedItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      Image.asset(
        fit: BoxFit.contain,
        'images/flutter.jpg',
        height: 130,
        width: 130,
      ),
      Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              "Title",
              style: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
            Text(" "),
            Text(
              "Artist",
              style: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
          ]),
    ]);
  }
}
