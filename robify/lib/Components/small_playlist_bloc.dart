import 'package:flutter/material.dart';
import 'package:robify/Components/small_stylizers.dart';

class SmallPlaylistbloc extends StatefulWidget {
  const SmallPlaylistbloc({Key? key}) : super(key: key);

  @override
  State<SmallPlaylistbloc> createState() => _SmallPlaylistblocState();
}

class _SmallPlaylistblocState extends State<SmallPlaylistbloc> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Color.fromRGBO(48, 48, 48, 1),
        ),
        child: SizedBox(
          height: 64,
          child: Row(
            children: [
              Image.asset(
                'images/dash.jpg',
                fit: BoxFit.fitHeight,
                height: 64,
                width: 64,
              ),
              const SmallSpacer(
                spacerType: SpacerType.horizontal,
                spacing: 16,
              ),
              const Text(
                "Playlist",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
