import 'package:flutter/material.dart';

class SmallSpacer extends StatefulWidget {
  final double? spacing;
  final SpacerType spacerType;
  const SmallSpacer({Key? key, required this.spacing, required this.spacerType})
      : super(key: key);

  @override
  State<SmallSpacer> createState() => _SmallSpacerState();
}

class _SmallSpacerState extends State<SmallSpacer> {
  @override
  Widget build(BuildContext context) {
    switch (widget.spacerType) {
      case SpacerType.horizontal:
        return SizedBox(width: widget.spacing);
      case SpacerType.vertical:
        return SizedBox(height: widget.spacing);
    }
  }
}

enum SpacerType { horizontal, vertical }
