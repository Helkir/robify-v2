import 'package:flutter/material.dart';

class PlayerPage extends StatelessWidget {
  const PlayerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: () {
          debugPrint("PLAYER !");
        },
        child: const Text('PLAYER'),
      ),
    );
  }
}
